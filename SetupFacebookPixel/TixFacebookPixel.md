# Integration with Facebook Pixel, Product Catalog, Custom Audience and more

To enable Facebook Integration with Box Office go to Module > FacebookIntegration and follow the process to integrate your Facebook Business Manager with Tix Box Office

# Events/Data sent to Facebook

The following screenshot show when in the buying flow each Facebook Pixel Event is triggered and the code below shows what data is sent with the event

## View Production

![](../ViewProduction.png)

```js
fbq('trackCustom', 'ViewProduction', {
	content_ids: ['8406'],
	content_name: 'Salvador Sobral ',
	content_type: 'product_group'
})
```

## Select Event Group

![](../SelectEventGroup.png)

```js
fbq('track', 'ViewCategory', {
	content_ids: ['8406'],
	content_name: 'Salvador Sobral ',
	content_type: 'product_group'
})
```

## Select Event Date

![](../SelectEventDate.png)

```js
fbq('track', 'ViewContent', {
	content_ids: ['39015'],
	value: 12990,
	num_items: 1,
	content_name: 'Salvador Sobral  - 01. nóv. 2019 - fös.',
	content_type: 'product',
	currency: 'ISK'
})
```

## Begin checkout

![](../AddToCart.png)

Add To Cart even can trigger at the product page if that is applicable

```js
fbq('track', 'AddToCart', {
	content_ids: ['39015'],
	value: 12990,
	num_items: 1,
	content_name: 'Salvador Sobral  - 01. nóv. 2019 - fös.',
	content_type: 'product',
	currency: 'ISK'
})
```

```js
fbq('track', 'InitiateCheckout', {
	content_ids: ['39015'],
	value: 12990,
	num_items: 1,
	content_name: 'Salvador Sobral  - 01. nóv. 2019 - fös.',
	content_type: 'product',
	currency: 'ISK'
})
```

## Add Payment Info

![](../AddPaymentInfo.png)

```js
gtag('event', 'AddPaymentInfo', {
	content_ids: ['39015'],
	value: 12990,
	num_items: 1,
	content_name: 'Salvador Sobral  - 01. nóv. 2019 - fös.',
	content_type: 'product',
	currency: 'ISK'
})
```

## Purchase finished

![](../Purchase.png)

```js
gtag('event', 'Purchase', {
	content_ids: ['39015'],
	value: 12990,
	num_items: 1,
	content_name: 'Salvador Sobral  - 01. nóv. 2019 - fös.',
	content_type: 'product',
	currency: 'ISK'
})
```
