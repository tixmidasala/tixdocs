# Tix Event API

Contact Tix support staff to receive Base API Path for your Account

## EventGroup

The api provides a list of all available Event Groups and each Event Group includes 1 or more Event Dates

| Property name        | Description                                |
| -------------------- | ------------------------------------------ |
| `EventGroupId`       | Id of Event Group                          |
| `Name`               | Name of Event Group in Native Language     |
| `NameEnglish`        | Name of Event Group in English             |
| `SubTitle`           | Subtitle of Event Group                    |
| `SubTitleEnglish`    | Subtitle of Event Group in English         |
| `Description`        | Description of Event Group (HTML text)     |
| `DescriptionEnglish` | Name of Event Group in English (HTML text) |
| `EventImagePath`     | Main Event Image URL                       |
| `FeaturedImagePath`  | Feature Image URL                          |
| `PurchaseUrl`        | Purchase link for all dates of event group |
| `PurchaseUrlEnglish` | Purchase link in English                   |

## EventDate

| Property name               | Description                                                                             |
| --------------------------- | --------------------------------------------------------------------------------------- |
| `Categories`                | Comma seperated list of event categories                                                |
| `Tags`                      | Comma seperated list of event tags                                                      |
| `MinPrice`                  | Minimum online ticket price                                                             |
| `MaxPrice`                  | Maximum online ticket price                                                             |
| `ProductPurchaseUrl`        | URL to products sold with the event date                                                |
| `ProductPurchaseUrlEnglish` | URL to products sold with the event date in English                                     |
| `Products`                  | Array of all products sold at the eventDate                                             |
| `EventId`                   | The id of the event date                                                                |
| `Name`                      | The name of the event date                                                              |
| `StartDate`                 | Start of the event date                                                                 |
| `StartDateFormat`           | Start Date Format                                                                       |
| `EndDate`                   | End of the event date                                                                   |
| `EndDateFormat`             | End Date Format                                                                         |
| `WaitingList`               | Is there a waiting list for this event                                                  |
| `Venue`                     | Venue Name                                                                              |
| `Hall`                      | Hall Name                                                                               |
| `Promoter`                  | Promoter name                                                                           |
| `SoldOut`                   | Depricated, set to true if event is sold out                                            |
| `Duration`                  | Description of duration of event                                                        |
| `SaleStatus`                | 0 = "No Status", 1 = "Few Tickets", 2= "Sold Out", 3 = "Cancelled", 4 = "Not Scheduled" |
| `SaleStatusText`            | Description of SaleStatus                                                               |
| `PurchaseUrl`               | Purchase link for this certain Event Date                                               |
| `PurchaseUrlEnglish`        | Purchase link for this certain Event Date in English                                    |

## Example

```json
[
	{
		"Dates": [
			{
				"Categories": "Theatre, Music, Exhibition",
				"Tags": "",
				"MinPrice": 4500,
				"MaxPrice": 4500,
				"ProductPurchaseUrl": "",
				"ProductPurchaseUrlEnglish": "",
				"Products": [],
				"EventId": 42079,
				"Name": "ÐE LÓNLÍ BLÚ BOJS - söngleikurinn",
				"StartDate": "/Date(1573934400000)/",
				"StartDateFormat": "2019-11-16T20:00:00+00:00",
				"EndDate": "/Date(1573941600000)/",
				"EndDateFormat": "2019-11-16T22:00:00+00:00",
				"WaitingList": false,
				"Venue": "Hof Menningarhús",
				"Hall": "Hof-Hamraborg",
				"Promoter": "Aðrir viðburðahaldarar",
				"SoldOut": false,
				"Duration": "",
				"SaleStatus": 0,
				"SaleStatusText": "NoStatus",
				"PurchaseUrl": "https://tix.is/is/mak/buyingflow/tickets/9067/42079",
				"PurchaseUrlEnglish": "https://tix.is/en/mak/buyingflow/tickets/9067/42079"
			}
		],
		"EventGroupId": 9067,
		"Name": "ÐE LÓNLÍ BLÚ BOJS - söngleikurinn",
		"NameEnglish": "",
		"SubTitle": "Hof Akureyri || 16.11.2019",
		"SubTitleEnglish": "",
		"Description": "<p></p><p>Söngleikurinn <i>Ðe Lónlí\r\nBlú Bojs</i> segir frá ungum drengjum, þeim Sörla, Páli og Njáli. Saman skipa\r\nþeir lítinn sönghóp með stóra\r\ndrauma. Þeir koma smáskífu til úrelts umboðsmanns að nafni Þorvaldur og landa\r\nhjá honum samningi. Þorvaldi finnst hljómsveitin ófullkomnuð og bætir inn honum\r\nValdimari, og þannig verður hljómsveitin Ðe Lónlí Blú Bojs til. Sagan\r\nsegir frá því hvernig bandið þróast þegar þessi nýji meðlimur er skipaður inn,\r\nhvernig ástin getur flækst fyrir og hvernig frægðin getur stigið manni til\r\nhöfuðs.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Söngleikurinn er með lögum\r\nfrá Ðe Lónlí Blú Bojs sem allir ættu að kannast við, þar á meðal <i>Heim í\r\nBúðardal</i>, <i>Harðsnúna Hanna</i> og <i>Diggi Liggi Ló</i>.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Söngleikur sem fólk á öllum\r\naldri hefur gaman af!</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><b>Leikendur:</b></p>\r\n\r\n<p>Agla Bríet Einarsdóttir</p>\r\n\r\n<p>Berglind Alda Ástþórsdóttir</p>\r\n\r\n<p>Ingi Þór Þórhallsson</p>\r\n\r\n<p>Jón Svavar Jósefsson</p>\r\n\r\n<p>Mímir Bjarki Pálmason</p>\r\n\r\n<p>Styr Orrason</p>\r\n\r\n<p>Vilberg Andri Pálsson</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><b>Leikstjóri og\r\nhandritshöfundur:</b></p>\r\n\r\n<p>Höskuldur Þór Jónsson</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><b>Lög og lagatextar:</b></p>\r\n\r\n<p>Ðe Lónlí Blú Bojs </p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><b>Sýninga- og\r\nframleiðslustjórn:</b></p>\r\n\r\n<p>Máni Huginsson</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><b>Tónlistarstjórn:</b></p>\r\n\r\n<p>Kristján Sturla Bjarnason</p>\r\n\r\n<p>Brynjar Unnsteinsson</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><b>Söngstjóri:</b></p>\r\n\r\n<p>Ásgrímur Geir Logason</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><b>Ljósahönnun:</b></p>\r\n\r\n<p>Pálmi Jónsson</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><b>Ljósmyndun og plakatgerð:</b></p>\r\n\r\n<p>Stefanía Elín Linnet</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><b>Sviðsmaður og leikmunavörður:</b></p>\r\n\r\n<p>Karla Kristjánsdóttir</p>\r\n\r\n\r\n\r\n\r\n\r\n<br><p></p>",
		"DescriptionEnglish": "",
		"EventImagePath": "https://cdn.tix.is/tix/EventImages/Event_9067.jpg",
		"FeaturedImagePath": "https://cdn.tix.is/tix/EventImages/Feature_9067.jpg",
		"PurchaseUrl": "https://tix.is/is/mak/buyingflow/tickets/9067/",
		"PurchaseUrlEnglish": "https://tix.is/en/mak/buyingflow/tickets/9067/"
	}
]
```
