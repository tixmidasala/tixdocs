# Integration with Google Analytics

To enable google analytics integration register Google Analytics property id in Box Office

To make your Google Analytics account fully compliant with the analytics data we provide please follow the steps below.

### 1. Enable Enhanced Ecommerce in Google Analytics Settings

![](EnhancedEcommerce.png)

### 2. Define the following Custom Dimensions in Google Analytics Settings. They must have the same index as shown in the picture below

![](CustomDimensions.png)

| Index | Name             | Scope   |
| ----- | ---------------- | ------- |
| 1     | EventGroupName   | Product |
| 2     | EventGroupId     | Product |
| 3     | EventDateName    | Product |
| 4     | EventDateId      | Product |
| 5     | WebUserId        | Product |
| 6     | Price            | Product |
| 7     | TicketTypeId     | Product |
| 8     | Quantity         | Product |
| 9     | ViewEventGroupId | User    |
| 10    | ViewEventId      | User    |
| 11    | ViewWebUserId    | User    |
| 12    | FBStatus         | User    |

# Production page on another domain

Many of our clients have their own website and have a production page for events.
Most customers start their shopping experience on that production page.
To be able to track customer behavior properly, [Cross-domain measurement](https://developers.google.com/analytics/devguides/collection/gtagjs) must be enabled.

This can be enabled by including the following code on each production page.

Replace `YOUR_GA_MEASUREMENT_ID` with your own Google Analytics ID.

Replace `TIX_GA_MEASUREMENT_ID` and `COUNTRY_SPECIFIC_TIX_URL` with the respective texts found in the table below

| Country       | `TIX_GA_MEASUREMENT_ID` | `COUNTRY_SPECIFIC_TIX_URL` |
| ------------- | ----------------------- | -------------------------- |
| Iceland       | `UA-44358330-2`         | tix.is                     |
| Denmark       | `UA-44358330-9`         | tix.dk                     |
| Norway        | `UA-44358330-7`         | tix.no                     |
| Sweden        | `UA-44358330-6`         | tix.se                     |
| Faroe Islands | `UA-44358330-10`        | tix.fo                     |

Replace `EVENT_GROUP_ID` with the EventGroupID of the production

```js
gtag('config', 'YOUR_GA_MEASUREMENT_ID', {
	linker: {
		domains: ['COUNTRY_SPECIFIC_TIX_URL']
	},
	custom_map: {
		dimension1: 'EventGroupName',
		dimension2: 'EventGroupId'
	}
})

gtag('config', 'TIX_GA_MEASUREMENT_ID', {
	linker: {
		domains: ['COUNTRY_SPECIFIC_TIX_URL']
	},
	custom_map: {
		dimension1: 'EventGroupName',
		dimension2: 'EventGroupId'
	}
})

gtag('event', 'view_production', {
	event_category: 'ecommerce',
	ViewEventGroupId: 'EVENT_GROUP_ID'
})
```

# Events/Data sent to Google Analytics by Tix

The following screenshot show when in the buying flow each Google Analytics Event is triggered and the code below shows what data is sent with the event

## View Production

![](../ViewProduction.png)

```js
gtag('event', 'view_production', {
	event_category: 'ecommerce',
	ViewEventGroupId: '8644',
	ViewWebUserId: '0'
})
```

## Select Event Group

![](../SelectEventGroup.png)

```js
gtag('event', 'select_eventgroup', {
	event_category: 'ecommerce',
	ViewEventGroupId: '8118',
	ViewWebUserId: 0
})
```

## Select Event Date

![](../SelectEventDate.png)

```js
gtag('event', 'select_eventdate', {
	event_category: 'ecommerce',
	ViewEventId: '36931',
	ViewWebUserId: '0'
})
```

## View Item List

![](../ViewItemList.png)

```js
gtag('event', 'view_item_list', {
	items: [
		{
			id: EventDateId + TicketTypeId,
			name: EventDateName + TicketTypeName,
			location_id: Venue,
			brand: Promoter,
			category: EventGroupId,
			variant: eventDateId,
			dimension1: EventGroupName,
			dimension2: EventGroupId,
			dimension3: EventDateName,
			dimension4: EventDateId,
			dimension5: webUser,
			dimension6: price,
			dimension7: TicketTypeId,
			dimension8: quantity
		}
	]
})
```

Add To Cart can trigger at the product page if applicable otherwise it triggers when user clicks "Continue ->" after choosing tickets

```js
gtag('event', 'add_to_cart', {
	items: [
		{
			id: EventDateId + TicketTypeId,
			name: EventDateName + TicketTypeName,
			location_id: Venue,
			brand: Promoter,
			category: EventGroupId,
			variant: eventDateId,
			dimension1: EventGroupName,
			dimension2: EventGroupId,
			dimension3: EventDateName,
			dimension4: EventDateId,
			dimension5: webUser,
			dimension6: price,
			dimension7: TicketTypeId,
			dimension8: quantity
		}
	]
})
```

## Begin checkout

![](../BeginCheckout.png)

```js
gtag('event', 'begin_checkout', {
	items: [
		{
			id: EventDateId + TicketTypeId,
			name: EventDateName + TicketTypeName,
			location_id: Venue,
			brand: Promoter,
			variant: eventDateId,
			quantity: quantity,
			price: '7800',
			dimension1: EventGroupName,
			dimension2: EventGroupId,
			dimension3: EventDateName,
			dimension4: EventDateId,
			dimension5: webUser,
			dimension6: price,
			dimension7: TicketTypeId,
			dimension8: quantity
		}
	]
})
```

## Purchase finished

![](../Purchase.png)

```js
gtag('event', 'purchase', {
	transaction_id: TransactionId,
	value: totalValue,
	items: [
		{
			id: EventDateId + TicketTypeId,
			name: EventDateName + TicketTypeName,
			location_id: Venue,
			brand: Promoter,
			variant: eventDateId,
			quantity: quantity,
			price: '7800',
			dimension3: EventDateName,
			dimension4: EventDateId,
			dimension5: webUser,
			dimension6: price,
			dimension7: TicketTypeId,
			dimension8: quantity
		}
	]
})
```
