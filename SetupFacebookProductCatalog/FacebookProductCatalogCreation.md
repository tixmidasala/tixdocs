# How to create a Facebook Product Catalog

## 1. Open business.facebook.com and navigate to Catalog Manager

## 2. Create Catalog

Make sure you select E-commerce

![](Step1.png)

## 3. Name your Catalog

Name your Catalog and select the business that should owns this catalog, preferably the Facebook Business of the culture house.

![](Step2.png)

## 4. Select Product Data sources

![](Step3.png)

## 5. Select Add products and choose Use Data Feeds

![](Step4.png)

## 6. Setup Data Feed

Select Set Automatic File Upload Schedule and copy the link from BoxOffice and paste into Data Feed URL. Choose how often the catalog is updated, preferably hourly to prevent facebook from showing sold out events.

![](Step5.png)

## 7. Connect pixel to catalog

Navigate to Event data sources and select Connect to Tracking and finally select a Facebook Pixel to connect to your Facebook Catalog

![](Step7.png)
